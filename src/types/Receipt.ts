import type { Member } from "./Member";
import type { ReceiptItem } from "./RecieptItem";
import type { User } from "./User";


type Receipt =  {
    id: number;
    createdDate: Date;
    totalBefor: number;
    memberDiscount: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    ReceiptItems?: ReceiptItem[]
}

export type { Receipt }
