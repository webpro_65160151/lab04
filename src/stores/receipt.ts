import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/RecieptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'



export const useReceiptStore = defineStore('receipt', () => {
    const authStore = useAuthStore()
    const memberStore = useMemberStore()
    const receiptDialog = ref(false)

    const receipt = ref<Receipt>({
        id: 0,
        createdDate: new Date(),
        totalBefor: 0,
        memberDiscount: 0,
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: 'cash',
        userId: authStore.currentUser.id,
        user: authStore.currentUser,

        memberId: 0
    })

    const receiptItems = ref<ReceiptItem[]>([])

function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
        receiptItems.value[index].unit++
        calReceipt()
        return
    } else {
        const newReceipt: ReceiptItem = {
            id: -1,
            name: product.name,
            price: product.price,
            unit: 1,
            productId: product.id,
            product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()
    }
}

function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
}

function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
}
function dec(item: ReceiptItem) {
    if (item.unit === 1) {
        removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
}

function calReceipt() {
    let totalBefore = 0
    for(const item of receiptItems.value) {
        totalBefore = totalBefore + (item.price * item.unit)
    }
    receipt.value.totalBefor = totalBefore
    receipt.value.memberDiscount = 0
    if (memberStore.currentMember) {
        receipt.value.memberDiscount = totalBefore - (totalBefore * 0.95)
        receipt.value.total = totalBefore * 0.95 }
        
    else {
        receipt.value.total = totalBefore }
  }

  function showReceiptDialog() {
    receipt.value.ReceiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
        id: 0,
        createdDate: new Date(),
        totalBefor: 0,
        memberDiscount: 0,
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: 'cash',
        userId: authStore.currentUser.id,
        user: authStore.currentUser,
        memberId: 0
    }
    memberStore.clear()
  }


  return {addReceiptItem, receiptDialog, removeReceiptItem, inc, dec, receiptItems ,receipt, calReceipt , showReceiptDialog, clear }
})
